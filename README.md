
# Grammatical functions, inflectional class and textual frequency in Russian nominals

RES-000-23-0082

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.10462727.svg)](https://doi.org/10.5281/zenodo.10462727) [![pipeline](https://gitlab.com/surrey_morphology_group/paradigms-in-use/badges/main/pipeline.svg)](https://gitlab.com/surrey_morphology_group/paradigms-in-use/-/pipelines)


The following datasets were created for the ESRC-funded project '[Paradigms in use](https://www.smg.surrey.ac.uk/projects/paradigms/)' (RES-000-23-0082):

The datasets are in the form of 8 EXCEL spreadsheets. The lexemes recorded in the datasets are those represented by word forms occurring in total at least five times. Lexemes occurring less than five times were excluded to avoid large standard errors in the estimates which occur when observed numbers in each category are small (Corbett, Hippisley, Brown and Marriott 2001:208). Below a
brief summary of the contents of each spreadsheet is given.

This work is based on two corpora (The Russian Standard corpus and the Uppsala corpus) and is released under [Attribution-NonCommercial-ShareAlike 4.0 International  (CC BY-NC-SA)](http://creativecommons.org/licenses/by-nc-sa/4.0/).

## Citation 

Carole Tiberius, Dunstan Brown, Greville G. Corbett, Trevor Sweeting and Peter Williams. 2015. Grammatical functions, inflectional class and textual frequency in Russian nominals (dataset). https://doi.org/10.5281/zenodo.10462727

## Files

This dataset encloses the following files:

| Dataset citation name                                                                | File                            |
|--------------------------------------------------------------------------------------|---------------------------------|
| Uppsala Corpus - Nouns dataset                                                       | Uppsala-nouns.xls               |
| Uppsala Corpus - Pronouns dataset                                                    | Uppsala-pronouns.xls            |
| Russian Standard Corpus 1 - Adjectives dataset                                       | RussianStandard1-adjectives.xls |
| Russian Standard Corpus 1 - Nouns dataset                                            | RussianStandard1-nouns.xls      |
| Russian Standard Corpus 1 - Pronouns dataset                                         | RussianStandard1-pronouns.xls   |
| Russian Standard Corpus 2 - Adjectives dataset                                       | RussianStandard2-adjectives.xls |
| Russian Standard Corpus 2 - Nouns dataset                                            | RussianStandard2-nouns.xls      |
| Russian Standard Corpus 2 - Pronouns dataset                                         | RussianStandard2-pronouns.xls   |
| Form/function alignment in Russian adjectives (based on the Russian Standard Corpus) | adjectives-ijcl.pdf             |

## Corpus 1

Datasets derived from the [Uppsala corpus](https://www.lingexp.uni-tuebingen.de/sfb441/b1/en/korpora.html), comprising nouns (Uppsala-nouns.xls and Uppsala-pronouns.xls).

These datasets contain the following column headings:

| Column heading        | Column content                                          |
|-----------------------|---------------------------------------------------------|
| Lexeme-transliterated | transliterated lexeme                                   |
| NM class              | network morphology class                                |
| Zaliznjak             | Zaliznjak’s (1977) morphological type                   |
| Unicode Lexeme        | lexeme in unicode                                       |
| Gloss                 | gloss                                                   |
| Animacy               | animacy                                                 |
| NomSg                 | nominative singular                                     |
| AccSg                 | accusative singular                                     |
| Ref to NomSg          | reference to nominative singular (inanimate accusative) |
| GenSg                 | genitive singular                                       |
| Gen2Sg                | second genitive singular                                |
| DatSg                 | dative singular                                         |
| InstSg                | instrumental singular                                   |
| InstSg2               | second instrumental singular                            |
| LocSg                 | locative singular                                       |
| Loc2Sg                | second locative singular                                |
| Vocative              | vocative                                                |
| NomPl                 | nominative plural                                       |
| Nom2Pl                | second nominative plural                                |
| AccPl                 | accusative plural                                       |
| Ref to Nom1Pl         | reference to nominative plural (inanimate accusative)   |
| Acc2Pl                | second accusative plural                                |
| GenPl                 | genitive plural                                         |
| Gen2Pl                | second genitive plural                                  |
| DatPl                 | dative plural                                           |
| InstPl                | instrumental plural                                     |
| Inst2Pl               | second instrumental plural                              |
| LocPl                 | locative plural                                         |
| Frequency             | total frequency                                         |
| Sg                    | singular frequency                                      |
| Pl                    | plural frequency                                        |
| pl/freq               | relative plural frequency                               |

## Corpus 2 and 3

Datasets derived from the [Russian Standard Corpus](https://old.mccme.ru/ling/mitrius/article.html), comprising nouns, pronouns and adjectives. Each have slightly different headings. The datasets for nouns (RussianStandard1-nouns.xls and RussianStandard2-nouns.xls) contain the following headings:

| Column heading | Column content                        |
|----------------|---------------------------------------|
| Lexeme         | lexeme                                |
| Zaliznjak      | Zaliznjak’s (1977) morphological type |
| NM Class       | network morphology class              |
| Animacy        | animacy                               |
| NomSg          | nominative singular                   |
| GenSg          | genitive singular                     |
| Gen2Sg         | second genitive singular              |
| DatSg          | dative singular                       |
| AccSg          | accusative singular                   |
| InstrSg        | instrumental singular                 |
| LocSg          | locative singular                     |
| Loc2Sg         | second locative singular              |
| NomPl          | nominative plural                     |
| GenPl          | genitive plural                       |
| DatPl          | dative plural                         |
| AccPl          | accusative plural                     |
| InstrPl        | instrumental plural                   |
| LocPl          | locative plural                       |
| TOTAL          | total frequency                       |

The datasets for adjectives (RussianStandard1-adjectives.xls and RussianStandard2-adjectives.xls) contain the following headings:

| Column heading | Column content                        |
|----------------|---------------------------------------|
| Lexeme         | lexeme                                |
| Zaliznjak      | Zaliznjak’s (1977) morphological type |
| NM Class       | network morphology class              |
| Animacy        | animacy                               |
| NomSg          | nominative singular                   |
| GenSg          | genitive singular                     |
| Gen2Sg         | second genitive singular              |
| DatSg          | dative singular                       |
| AccSg          | accusative singular                   |
| InstrSg        | instrumental singular                 |
| LocSg          | locative singular                     |
| Loc2Sg         | second locative singular              |
| NomPl          | nominative plural                     |
| GenPl          | genitive plural                       |
| DatPl          | dative plural                         |
| AccPl          | accusative plural                     |
| InstrPl        | instrumental plural                   |
| LocPl          | locative plural                       |
| TOTAL          | total frequency                       |

# References

- Lönngren, Lennart. The Uppsala Russian Corpus (dataset). https://snd.gu.se/en/catalogue/dataset/ext0071-1. Query interface: https://www.lingexp.uni-tuebingen.de/sfb441/b1/en/korpora.html
- Lönngren, Lennart (ed.), 1993. Chastotnyj slovar' sovremennogo russkogo jazyka. (A Frequency Dictionary of Modern Russian. With a Summary in English.) Acta Universitatis Upsaliensis, Studia Slavica Upsaliensia 32. 188 pp. Uppsala. ISBN 91-554-3134-8.
- Sharoff, Serge (2006). "Methods and tools for development of the Russian Reference Corpus". In Corpus linguistics around the world. Leiden, The Netherlands: Brill. https://doi.org/10.1163/9789401202213_014