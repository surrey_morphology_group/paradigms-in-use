#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from pathlib import Path
import frictionless as fl
import pandas as pd

col_info = {'Lexeme': 'lexeme',
            'Zaliznjak': 'Zaliznjak’s (1977) morphological type',
            'NM Class': 'network morphology class',
            'Animacy': 'animacy',
            'NomSg': 'nominative singular',
            'GenSg': 'genitive singular',
            'Gen2Sg': 'second genitive singular',
            'DatSg': 'dative singular',
            'AccSg': 'accusative singular',
            'InstrSg': 'instrumental singular',
            'LocSg': 'locative singular',
            'Loc2Sg': 'second locative singular',
            'NomPl': 'nominative plural',
            'GenPl': 'genitive plural',
            'DatPl': 'dative plural',
            'AccPl': 'accusative plural',
            'InstrPl': 'instrumental plural',
            'LocPl': 'locative plural',
            'TOTAL': 'total frequency',
            'Lexeme-transliterated': 'transliterated lexeme',
            'NM class': 'network morphology class',
            'Unicode Lexeme': 'lexeme in unicode',
            'Gloss': 'gloss',
            'Ref to NomSg': 'reference to nominative singular (inanimate accusative)',
            'InstSg': 'instrumental singular',
            'InstSg2': 'second instrumental singular',
            'Vocative': 'vocative',
            'Nom2Pl': 'second nominative plural',
            'Ref to Nom1Pl': 'reference to nominative plural (inanimate accusative)',
            'Acc2Pl': 'second accusative plural',
            'Gen2Pl': 'second genitive plural',
            'InstPl': 'instrumental plural',
            'Inst2Pl': 'second instrumental plural',
            'Frequency': 'total frequency',
            'Sg': 'singular frequency',
            'Pl': 'plural frequency',
            'pl/freq': 'relative plural frequency'}

titles = {"Uppsala-nouns.xls": "Uppsala Corpus - Nouns dataset",
          "Uppsala-pronouns.xls": "Uppsala Corpus - Pronouns dataset",
          "RussianStandard1-adjectives.xls": "Russian Standard Corpus 1 - Adjectives dataset",
          "RussianStandard1-nouns.xls": "Russian Standard Corpus 1 - Nouns dataset",
          "RussianStandard1-pronouns.xls": "Russian Standard Corpus 1 - Pronouns dataset",
          "RussianStandard2-adjectives.xls": "Russian Standard Corpus 2 - Adjectives dataset",
          "RussianStandard2-nouns.xls": "Russian Standard Corpus 2 - Nouns dataset",
          "RussianStandard2-pronouns.xls": "Russian Standard Corpus 2 - Pronouns dataset",
          "adjectives-ijcl.pdf": "Form/function alignment in Russian adjectives (based on the Russian Standard Corpus)",
          }
resources = []

for data_file in Path("./").glob("*.xls"):
    title = titles[data_file.name]
    r = fl.Resource(data_file, title=title, name=data_file.stem.lower())
    resources.append(r)

package = fl.Package(resources=resources,
                     licenses=[{
                         "path": "http://creativecommons.org/licenses/by-sa/4.0/?ref=chooser-v1",
                         "title": "Creative Commons Attribution-ShareAlike 4.0 International"
                     }],
                     profile="data-package",
                     contributors=[{"title": "Tiberius, Carole"},
                                    {"title": "Brown, Dunstan"},
                                    {"title": "Corbett, Greville"},
                                    {"title": "Sweeting, Trevor"},
                                    {"title": "Williams, Peter"},
                                   ],
                     title="Grammatical functions, inflectional class and textual frequency in Russian nominals",
                     version="1.0.0",
                     homepage="https://doi.org/10.5281/zenodo.10462727",
                     keywords=["linguistics", "morphology", "paradigms", "Russian", "nouns"],
                     name="paradigms_in_use")
package.custom["id"] = "https://doi.org/10.5281/zenodo.10462727"
package.infer()

# Add titles to columns
for r in package.resources:
    for col in r.schema.fields:
        if col.name in col_info:
            col.title = col_info[col.name]

package.to_json("paradigms_in_use.package.json")
